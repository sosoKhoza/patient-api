package com.patient.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PatientApi2Application {

	public static void main(String[] args) {
		SpringApplication.run(PatientApi2Application.class, args);
	}

}
