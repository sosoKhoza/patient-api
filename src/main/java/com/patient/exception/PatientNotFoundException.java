package com.patient.exception;

public class PatientNotFoundException extends RuntimeException {
	public PatientNotFoundException(String exception) {
		super(exception);
	}

}
