package com.patient.repository;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.patient.domain.Patient;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long>{
	@Query("SELECT p FROM Patient p WHERE LOWER(p.name) = LOWER(:name)")
	Optional<Patient> getPatientByName(String name);
	@Query("SELECT p FROM Patient p WHERE LOWER(p.surname) = LOWER(:surname)")
	Optional<Patient> getPatientBySurname(String surname);
	@Query("SELECT p FROM Patient p WHERE LOWER(p.age) = LOWER(:age)")
	Optional<Patient> getPatientByAge(LocalDate age);
}
