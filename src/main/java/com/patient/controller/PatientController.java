package com.patient.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.patient.domain.Patient;
import com.patient.exception.PatientNotFoundException;
import com.patient.service.PatientService;

@RestController
@RequestMapping(value ="/patientApi")
public class PatientController {
	@Autowired
	private PatientService patientService;
	
	@GetMapping("/patient/search/{name}")
	public ResponseEntity<Patient> getPersonByName(@PathVariable("name") String name) {
		Optional<Patient> patient = patientService.getPatientByName(name);
		if (!patient.isPresent()) {
			throw new PatientNotFoundException("Patient with name - " + name + "does not exist");
		}
		return new ResponseEntity<Patient>(patient.get(), HttpStatus.OK);
	}
	
	@GetMapping("/patient/search/{surname}")
	public ResponseEntity<Patient> getPersonBySurname(@PathVariable("surname") String surname) {
		Optional<Patient> patient = patientService.getPatientBySurname(surname);
		if (!patient.isPresent()) {
			throw new PatientNotFoundException("Patient with surname - " + surname + "does not exist");
		}
		return new ResponseEntity<Patient>(patient.get(), HttpStatus.OK);
	}

}
