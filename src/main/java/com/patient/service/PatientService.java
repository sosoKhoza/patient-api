package com.patient.service;

import java.time.LocalDate;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.patient.domain.Patient;
import com.patient.repository.PatientRepository;

@Service
public class PatientService implements IPatientService {
	
	@Autowired
	private PatientRepository patientRepository;

	@Override
	public Optional<Patient> getPatientByName(String name) {
		return patientRepository.getPatientByName(name);
	}

	@Override
	public Optional<Patient> getPatientBySurname(String surname) {
		return patientRepository.getPatientBySurname(surname);
	}

	@Override
	public Optional<Patient> getPatientByAge(LocalDate age) {
		return patientRepository.getPatientByAge(age);
	}

}
