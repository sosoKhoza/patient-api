package com.patient.service;

import java.time.LocalDate;
import java.util.Optional;

import com.patient.domain.Patient;

public interface IPatientService {
	Optional<Patient> getPatientByName(String name);
	Optional<Patient> getPatientBySurname(String surname);
	Optional<Patient> getPatientByAge(LocalDate age);
}
